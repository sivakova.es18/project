bnn-hep
=======

A tool to enable Bayesian neural networks (BNN) for high-energy physics. The project is an adaptation of BNN from
[FBM package](http://www.cs.toronto.edu/~radford/fbm.software.html) developed by R.M. Neal, relevant fragments of which
are added to the repository. Key differences with respect to the original framework are the following:
 * Neural networks for problem of binary classification are only considered (no regression, only two classes of events
   are allowed)
 * Customisation is done in a convenient way with the help of a configuraion file. User runs a single executable
 * Events are described in [ROOT](http://root.cern.ch) files
 * Implementation can handle weighted events (though, only physical, sctrictly positive weights are allowed)
 * C++ code that defines a specific BNN is produced as the result of training

Configuration file is parsed with a pruned version of [libconfig](http://www.hyperrealm.com/libconfig/) library. The tool
uses [ROOT](http://root.cern.ch) and [Boost](http://www.boost.org) libraries.

Being a detached tool, bnn-hep cannot compete with a generalised MVA framework in the level of flexibility and convenience
of usage. In addition to it, development is complicated by the fact that original FBM framework is coded in pure C. For
this reason no active development of the tool is expected, but only bugfixes and minor improvements. In a longer time
scale I hope to reimplement BNN in [TMVA package](http://tmva.sourceforge.net/).

**Current version:**

    /afs/cern.ch/work/p/pvolkov/public/bnn_2/test_dnn 

**Install and use bnn:** 

_(on pccms168)_

    cp -R /current_version /workdir 

    cd /workdir 

    source env1.sh

    ./make-all 

    cd bnn-hep/test

    bnn-hep tWb.cfg

_tWb.cfg main parametrs:_ 

    task-name = "name_of_the_task" name of network and finish .hpp & .py files

        file-name = "/scratch/pvolkov/tWb/samples_new/13tev__tWb_DR1__GG_NneEeuUdDbB.root"; sample path
        number-events = ["25000", "50%"]; number of events <= persent of file 
        event-list-file = "qcd2_trainEvents.txt";

    signal and background can have few samples. In this case, you should specify them separated by commas.

    ensemble-size = 8; <= numbers of networks
    burn-in = 7; <= lenght of trainning


after trainning name_of_the_task.hpp and name_of_the_taskC.py must be written

**use dnn:**

_(on lxplus/pccms167/pccms267)_

    cd workdir/bnn-hep/test

    source dnn_env.sh 

    python trainModel.py --taskName name_of_the_task --layers 3 --externalEepoch 10 --internalEpoch 100 --modelName testModel

_taskName - name_of_the_task same the bnn taskName_

network architecture can be changed in model_arch.py

**use addDNN:**

_(on lxplus/pccms167/pccms267)_

    cd workdir/bnn-hep/test

    source dnn_env.sh 

    python addDNN.py --taskName name_of_the_task  --modelName testModel --branchName DNN_test_branch --treeName LHE

_Instead of LHE, the sample can have another name for the tree, for example, Vars_





